using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        public readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

         [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
           var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
           var vendas = _context.Vendas;
            return Ok(vendas);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });

            if (venda.ItemVendido == null)
                return BadRequest(new { Erro = "deve se ter algum item vendido" });

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia" });      

            vendaBanco.Vendendor = venda.Vendendor;
            vendaBanco.ItemVendido = venda.ItemVendido;
            vendaBanco.Status = venda.Status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok();
        }
    }
}