namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoParaTransportadora = 2,
        Entrege = 3,
        Cancelada = 4
    }
}