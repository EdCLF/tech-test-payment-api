using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string Vendendor { get; set; }
        public string ItemVendido { get; set; }
        public EnumStatusVenda Status { get; set; }
        public DateTime Data { get; set; }
    }
}